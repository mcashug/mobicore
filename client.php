<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/* Copyright MobiCore Developped by CYUSA ELOI */

//date_default_timezone_set('Africa/Nairobi');
//production
class Client extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('client_model');
        $this->load->model('agent_model');
        $this->load->model('sms_model');
        $this->load->model('vas_model');
        $this->load->model('login_model');
        $this->load->model('merchant_model');
        $this->load->model('sms_model');
        $this->load->model('sms_model');
        $this->load->model('unregistered_client_transfers');
        ini_set('max_execution_time', 1200);
        session_start();

    }

    public function index()
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/home/client';
            $data['home'] = 'index.php/client';
            $data['title'] = "MobiCore - Client Home";
            $data['content'] = 'homeView';

            $this->client_model->clt_balance();
            $data['menus'] = $this->client_model->tab_menus();
            $data['clt_transfer'] = $_SESSION['transfers'];
            $data['clt_trans'] = $_SESSION['clt_trans'];
            $data['clt_saving'] = $_SESSION['clt_saving'];
            $data['clt_dedict'] = $_SESSION['clt_dedict'];

            $this->load->view('client_home', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function transfer_to_mcash($msg = NULL)
    {
        $data['role'] = $_SESSION['role'];
        $data['username'] = $_SESSION['realname'];
        $data['link'] = 'index.php/client/transfer_to_mcash';
        $data['home'] = 'index.php/client';
        $data['operation'] = 'Transfer to another Client';

        $data['title'] = "MobiCore - Client Home";
        $data['content'] = 'modals/client_transfer_tomcash';

        $data['error_msg'] = $msg;
        $this->load->view('modals/master', $data);

    }

    public function transfer_process()
    {
        $data['username'] = $this->session->userdata('realname');
        $data['title'] = "MobiCore - Client Transfer";
        $data['link'] = 'index.php/client/transfer_to_mcash';
        $data['home'] = 'index.php/client';
        $data['operation'] = 'Transfer to another Client';

        $this->form_validation->set_rules('accountNumber', 'accountNumber', 'trim|required|xss_clean');
        $this->form_validation->set_rules('amount', 'amount', 'trim|required|xss_clean|alpha_numeric');

        if ($this->form_validation->run() === FALSE) {
            $msg = '<font color=red>Form not fully filled.</font> <br />';
            $this->transfer_to_mcash($msg);
        } else {

            $receivernumber = $this->input->post('accountNumber');
            $amount = $this->input->post('amount');

            $checked = $this->client_model->transfer_to_client($receivernumber, $amount);

            $validation = $checked->return->status;

            if ($validation == 'PROCESSED') {
                $data['amount'] = $checked->return->transfer->amount;
                $data['receiver_name'] = $checked->return->transfer->member->name;
                $data['sender_name'] = $checked->return->transfer->fromMember->name;
                $data['ticket_number'] = $checked->return->transfer->id;

                /*$array_data= $this->session->userdata('fields');
                foreach ($array_data as $item){
                    if($item->internalName == "mobilePhone"){
                        $sendernumber=$item->value;
                    }
                }
                $this->sendsms($receivernumber, $sendernumber);*/

                $data['content'] = 'modals/client_transfer_success';
                $this->login_model->getAccountStatus();
                $this->load->view('modals/master', $data);

            } else {
                $msg = '<font color=red>The transaction failed to process. ' . $validation . '</font> <br />';
                $this->transfer_to_mcash($msg);
            }

        }
    }

    public function marketplace($msg = NULL)
    {
        if (isset($_SESSION['principalType'])) {
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/marketplace';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Market Place';

            $data['title'] = "MobiCore - Client Home";
            $data['content'] = 'modals/client_marketplace';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function buyairtime($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/buyairtime';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Buy Airtime';

            $data['title'] = "MobiCore - Client Buy airtime";
            $data['content'] = 'client_modals/client_buyairtime';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function buyairtime_cfrm($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client';
            $data['again'] = 'index.php/client/buyairtime';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Buy Airtime';

            $data['title'] = "MobiCore - Client Buy airtime";

            //$this->form_validation->set_rules('mno_provider', 'Network Provider', 'trim|required|xss_clean');
            $this->form_validation->set_rules('receiver_number', 'Receiver Phone Number', 'trim|required|xss_clean|alpha_numeric');
            $this->form_validation->set_rules('conf_receiver_number', 'Confirm Phone Number', 'trim|required|xss_clean|alpha_numeric|matches[receiver_number]');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean|alpha_numeric');
            //$this->form_validation->set_rules('agent_pin','Agent Pin','trim|required|xss_clean|callback_check_agentpin');
            if ($this->form_validation->run() === FALSE) {
                $this->buyairtime();
            } else {
                $recnumber = $this->input->post('receiver_number');
                $amount = $this->input->post('amount');
                //$net_prov = $this->input->post('mno_provider');
                //Start the process of sending the info to the TELECO
                if (isset($recnumber)) {
                    //get a formatted number
                    $formated_number = $recnumber;
                    $principal = $formated_number;
                    $princ_type = "USER";
                    $transType = 54;
                    $paywayNumber = "payway";
                    $princtype = "USER";
                    $description = "AIRTIME FOR " . $recnumber . " amount:" . $amount;
                    //LOAD MERCHANT DETAILS
                    $merchant = $this->merchant_model->getMerchant("USER", "payway");
                    $url = $this->merchant_model->merchantapiurl;
                    $vendor_password = $this->merchant_model->merchantapipassword;
                    $vendor_code = $this->merchant_model->merchantapiusername;
                    if (isset($url) && isset($vendor_password) && isset($vendor_code)) {
                        $resp_payment = $this->agent_model->tansaction_process($paywayNumber, $princtype, $amount, $transType, $description);
                        if (!isset($resp_payment->return->status)) {
                            $msg = '<font color=red>Error message: The system is down please try again later. We apologize for any inconveniences.</font> <br />';
                            $this->buyairtime($msg);
                        }
                    } else {
                        $msg = '<font color=red>Error message: The system is down please try again later, we apologize for any inconveniences.</font> <br />';
                        $this->buyairtime($msg);
                    }
                    if (isset($resp_payment->return->status) && $resp_payment->return->status == 'PROCESSED') {
                        $buy_airtime = $this->vas_model->loadAirtime($amount, $formated_number, $resp_payment->return->transfer->id);


                        //do the cyclos transaction first to get the transaction id
                        $val = strstr($buy_airtime, "Success");
                        $val2 = strstr($buy_airtime, "Accepted");
                        if ($val || $val2) {
                            $data['phone_number'] = $formated_number;
                            $data['amount'] = $amount;
                            $data['content'] = 'client_modals/client_buyairtime_suc';
                            $this->login_model->getAccountStatus();
                            $this->load->view('modals/master', $data);
                        } else {
                            $cb = $this->client_model->chargeBack($resp_payment->return->transfer->id);
                            $msg = '<font color=red>Error message: ' . $buy_airtime . ' Please contact the administrator.</font> <br />';
                            $this->buyairtime($msg);

                        }
                    } else {
                        //$cb = $this->agent_model->chargeBack($resp_payment->return->transfer->id);
                        $msg = "<font color=red>Error message:AIRTIME FOR " . $recnumber . " amount:" . $amount . " loading was not successful and an error occurred in the process Please contact the administrator</font> <br />Error: " . $resp_payment->return->status;
                        log_message('error', "Error message:AIRTIME FOR " . $recnumber . " amount: " . $amount . " loading was not successful and an error occurred in the process Please contact the administrator");
                        $this->buyairtime($msg);
                    }

                }
                //$buy_airtime;

            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }


    public function pay_startimes($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/pay_startimes';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Buy Startimes';

            $data['title'] = "MobiCore - Client Home";
            $data['content'] = 'client_modals/client_subscr_startimes';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }

    }

    public function pay_startimes_process($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client';
            $data['again'] = 'index.php/client/pay_startimes';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Buy Startimes';

            $data['title'] = "MobiCore - Client Buy Startimes";

            $this->form_validation->set_rules('receiver_number', 'Phone Number', 'trim|required|xss_clean');
            $this->form_validation->set_rules('account_number', 'Receiver Phone Number', 'trim|required|xss_clean|alpha_numeric');
            $this->form_validation->set_rules('conf_account_number', 'Confirm Phone Number', 'trim|required|xss_clean|alpha_numeric|matches[account_number]');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean|alpha_numeric');

            if ($this->form_validation->run() === FALSE) {
                $this->pay_startimes();
                return;
            } else {
                $recnumber = $this->input->post('receiver_number');
                $amount = $this->input->post('amount');
                $account_number = $this->input->post('account_number');
                $conf_account_number = $this->input->post('conf_account_number');
                //Start the process of sending the info to the TELECO
                if ($account_number == $conf_account_number) {
                    //get a formatted number
                    $formated_number = $recnumber;
                    $principal = $formated_number;
                    $princ_type = "USER";
                    $transType = 54;
                    $paywayNumber = "payway";
                    $princtype = "USER";
                    $description = "STARTIMES FOR " . $account_number . " amount:" . $amount;
                    //LOAD MERCHANT DETAILS
                    $merchant = $this->merchant_model->getMerchant("USER", "payway");
                    $url = $this->merchant_model->merchantapiurl;
                    $vendor_password = $this->merchant_model->merchantapipassword;
                    $vendor_code = $this->merchant_model->merchantapiusername;
                    if (isset($url) && isset($vendor_password) && isset($vendor_code)) {
                        $resp_payment = $this->agent_model->tansaction_process($paywayNumber, $princtype, $amount, $transType, $description);
                        if (!isset($resp_payment->return->status)) {
                            $msg = '<font color=red>Error message: The system is down please try again later. We apologize for any inconveniences.</font> <br />';
                            $this->pay_startimes($msg);
                            return;
                        }
                    } else {
                        $msg = '<font color=red>Error message: The system is down please try again later, we apologize for any inconveniences.</font> <br />';
                        $this->pay_startimes($msg);
                        return;
                    }
                    if (isset($resp_payment->return->status) && $resp_payment->return->status == 'PROCESSED') {
                        $buy_startimes = $this->vas_model->loadstartimes($amount, $formated_number, $resp_payment->return->transfer->id, $account_number);

                        //do the cyclos transaction first to get the transaction id
                        $val = strstr($buy_startimes, "Success");
                        $val2 = strstr($buy_startimes, "Accepted");
                        if ($val || $val2) {
                            $data['phone_number'] = $formated_number;
                            $data['amount'] = $amount;
                            $data['account'] = $account_number;
                            $data['content'] = 'client_modals/client_subscr_startimes_suc';
                            $this->login_model->getAccountStatus();
                            $this->load->view('modals/master', $data);
                        } else {
                            $cb = $this->client_model->chargeBack($resp_payment->return->transfer->id);
                            $msg = '<font color=red>Error message: ' . $buy_startimes . ' Please contact the administrator.</font> <br />';
                            $this->pay_startimes($msg);
                            return;
                        }
                    } else {
                        //$cb = $this->agent_model->chargeBack($resp_payment->return->transfer->id);
                        $msg = "<font color=red>Error message: Startimes topup FOR " . $account_number . " of amount: " . $amount . "UGX loading was not successful and an error occurred in the process Please contact the administrator</font> <br />Error: " . $resp_payment->return->status;
                        log_message('error', "Error message: Startimes topup FOR " . $account_number . " amount: " . $amount . "UGX loading was not successful and an error occurred in the process Please contact the administrator");
                        $this->pay_startimes($msg);
                        return;
                    }

                }
                //$buy_airtime;

            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }


    public function buyumeme($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/buyumeme';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Buy Umeme';

            $data['title'] = "MobiCore - Client Buy umeme";
            $data['content'] = 'client_modals/client_buyumeme';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function buyumeme_st($msg = NULL)
    {
        global $verify_umeme;
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/buyumeme_st';
            $data['again'] = 'index.php/client/buyumeme';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Buy Umeme';

            $data['title'] = "MobiCore - Client Buy umeme";
            $this->form_validation->set_rules('customer_no', 'Customer Number', 'trim|required|xss_clean', 'required');
            $this->form_validation->set_rules('receiver_number', 'Receiver Phone Number', 'trim|required|xss_clean|alpha_numeric', 'required');
            if ($this->form_validation->run() === FALSE) {
                $this->buyumeme();
            } else {
                $custno = $this->input->post('customer_no');
                $recnumber = $this->input->post('receiver_number');
                $_SESSION['recnumber'] = $recnumber;
                $_SESSION['custno'] = $custno;

                //Start the process of sending the info to the TELECO
                if (isset($custno)) {
                    //get a formatted number
                    $principal = $_SESSION['Username'];
                    $princ_type = "USER";
                    $paywayNumber = "payway";
                    $action = "verify";
                    $princtype = "USER";
                    $description = "UMEME VAERIFICATION FOR " . $recnumber;
                    //LOAD MERCHANT DETAILS
                    $merchant = $this->merchant_model->getMerchant("USER", "payway");
                    $url = $this->merchant_model->merchantapiurl;
                    $vendor_password = $this->merchant_model->merchantapipassword;
                    $vendor_code = $this->merchant_model->merchantapiusername;

                    if (!isset($url) && !isset($vendor_password) && !isset($vendor_code)) {
                        $msg = '<font color=red>Error message: The system is down please try again later. We apologize for any inconveniences.</font> <br />';
                        $this->buyumeme($msg);
                    }

                    if ($action == "verify") {
                        $verify_umeme = $this->vas_model->query_customer($custno);
                        $val3 = strstr($verify_umeme, 'Success');
                        if ($val3) {
                            $data['phone_number'] = $principal;
                            $data['content'] = 'client_modals/client_buyumeme_fin';
                            $this->login_model->getAccountStatus();

                            $this->load->view('modals/master', $data);

                        } else {
                            $msg = '<font color=red>Error message: ' . $verify_umeme . ' Please contact the administrator.</font> <br />';
                            $this->buyumeme($msg);
                        }


                    }

                }

            }

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function buyumeme_fin($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client';
            //$data['again'] = 'index.php/client/buyumeme_st';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Buy Umeme';

            $data['title'] = "MobiCore - Client Buy umeme";

            $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean|alpha_numeric', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->buyumeme_st();
            } else {
                $amount = $this->input->post('amount');

                if (isset($amount)) {
                    //get a formatted number
                    $principal = $_SESSION['Username'];
                    $princ_type = "USER";
                    // TransferType Changed by Abdu
                    // $transType = 55;
                    $transType = 88;
                    //$transType = NULL;
                    $paywayNumber = "payway";
                    $action = "pay";
                    $princtype = "USER";
                    $description = "UMEME PAYMENT FOR " . $_SESSION['recnumber'];
                    //LOAD MERCHANT DETAILS
                    $merchant = $this->merchant_model->getMerchant("USER", "payway");
                    $url = $this->merchant_model->merchantapiurl;
                    $vendor_password = $this->merchant_model->merchantapipassword;
                    $vendor_code = $this->merchant_model->merchantapiusername;

                    if (!isset($url) && !isset($vendor_password) && !isset($vendor_code)) {
                        $msg = '<font color=red>Error message: The system is down please try again later. We apologize for any inconveniences.</font> <br />';
                        $this->buyumeme($msg);
                    }

                    if ($action == "pay") {
                        $buy_umeme = $this->agent_model->tansaction_process($paywayNumber, $princtype, $amount, $transType, $description);

                        if (!isset($buy_umeme->return->status)) {
                            $msg = '<font color=red>Error message: The system is down please try again later. We apologize for any inconveniences.</font> <br />';
                            $this->buyumeme($msg);
                        }

                        if (isset($buy_umeme->return->status) && $buy_umeme->return->status == 'PROCESSED') {
                            $val3 = strstr($this->vas_model->query_customer($_SESSION['custno']), 'Success');

                            if ($val3) {
                                $call_umeme = $this->vas_model->umemeYaka($amount, $_SESSION['recnumber'], $buy_umeme->return->transfer->id);
                                $val4 = strstr($call_umeme, 'Success');
                                $val5 = strstr($call_umeme, 'Accepted');
                                if ($val4 || $val5) {
                                    $_SESSION['response'] = $call_umeme;
                                    $data['content'] = 'client_modals/client_buyumeme_suc';
                                    $this->login_model->getAccountStatus();
                                    $this->load->view('modals/master', $data);
                                } else {
                                    $cb = $this->client_model->chargeBack($buy_umeme->return->transfer->id);
                                    $msg = '<font color=red>Error message: ' . $call_umeme . ' Please contact the administrator.</font> <br />';
                                    $this->buyumeme($msg);
                                }

                            } else {
                                $msg = '<font color=red>Customer not found.</font> <br />';
                                $this->buyumeme($msg);
                            }
                        } else {

                            $msg = '<font color=red>Error message: ' . $buy_umeme->return->status . ' Please contact the administrator.</font> <br />';
                            $this->buyumeme($msg);

                        }
                    }

                }

            }

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function pay_tvsubscr_menu($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/pay_tvsubscr_menu';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'DSTV Subscription';

            $data['title'] = "MobiCore - Choose DSTV/GOtv";
            $data['content'] = 'client_modals/client_tvsubscr_menu';

            $data['error_msg'] = $msg;

            $options = array(
                '' => 'Select',
                'DSTV.Primary' => 'DSTV',
                'GOtv' => 'GOtv'
            );
            $data['options'] = $options;
            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function pay_tvsubscr_provider($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/pay_tvsubscr_provider';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'DSTV/GOtv Subscription';
            $data['title'] = "MobiCore - Pay DSTV/GOtv";
            //$data['content'] = 'client_modals/client_tvsubscr_menu';
            $this->form_validation->set_rules('provider', 'Provider', 'trim|required|xss_clean', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->pay_tvsubscr_menu();
            } else {
                $provider = $this->input->post('provider');
                $data['error_msg'] = $msg;

                $merchant = $this->merchant_model->getMerchant("USER", "payway");
                $url = $this->merchant_model->merchantapiurl;
                $vendor_password = $this->merchant_model->merchantapipassword;
                $vendor_code = $this->merchant_model->merchantapiusername;

                $priceList = $this->vas_model->getTvPriceList($provider);

                //$data['options'] = $options;
                if ($provider == 'GOtv') {
                    $data['content'] = 'client_modals/client_tvsubscr_gotv';
                    $_SESSION['tv'] = $provider;
                } else if ($provider == 'DSTV.Primary') {
                    $data['content'] = 'client_modals/client_tvsubscr_dstv';
                    $_SESSION['tv'] = 'DSTV';
                }
                $data['priceList'] = $priceList;
                $this->load->view('modals/master', $data);
            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function pay_tvsubscr_gotv($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/pay_tvsubscr_gotv';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'GOtv Subscription';

            $data['title'] = "MobiCore - Client Pay GOtv";

            $this->form_validation->set_rules('gotv_acc', 'GOtv account', 'trim|required|xss_clean|required');
            $this->form_validation->set_rules('bouquet_choice', 'Bouquet Choice', 'trim|required|xss_clean', 'required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean|required');
            $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|xss_clean|required');

            if ($this->form_validation->run() === FALSE) {
                $this->pay_tvsubscr_menu();
            } else {
                $smartcardNo = $this->input->post('gotv_acc');
                $bouquet = $this->input->post('bouquet_choice');
                $amount = $this->input->post('amount');
                $phonenumber = $this->input->post('phone');

                if (isset($smartcardNo)) {

                    $principal = $_SESSION['Username'];
                    $princ_type = "USER";
                    $paywayNumber = "payway";
                    $action = "verify";

                    //LOAD MERCHANT DETAILS
                    $merchant = $this->merchant_model->getMerchant("USER", "payway");
                    $url = $this->merchant_model->merchantapiurl;
                    $vendor_password = $this->merchant_model->merchantapipassword;
                    $vendor_code = $this->merchant_model->merchantapiusername;

                    if (!isset($url) && !isset($vendor_password) && !isset($vendor_code)) {
                        $msg = '<font color=red>Error message: The system is down please try again later. We apologize for any inconveniences.</font> <br />';
                        $this->pay_tvsubscr_menu($msg);
                    }

                    if ($action == "verify") {
                        $verify_gotv = $this->vas_model->query_tv_customer($smartcardNo, $url, $vendor_code, $vendor_password);
                        $val6 = strstr($verify_gotv, 'Success');
                        if ($val6) {
                            $data['phone_number'] = $principal;
                            $_SESSION['amount'] = $amount;
                            $_SESSION['bouquet'] = $bouquet;
                            $_SESSION['smartcard_no'] = $smartcardNo;
                            $_SESSION['recno'] = $phonenumber;
                            //$data['amount'] = $amount;
                            $data['dstv_cust_name'] = $_SESSION['dstv_cust_name'];
                            //$data['content'] = 'client_modals/client_tvsubscr_gotv_fin';
                            $this->login_model->getAccountStatus();
                            //$this->load->view('modals/master', $data);

                        } else {
                            $msg = '<font color=red>Error message: ' . $verify_gotv . ' Please contact the administrator.</font> <br />';
                            $this->pay_tvsubscr_menu($msg);
                        }


                    }

                }

                $data['content'] = 'client_modals/client_tvsubscr_gotv_fin';
                $data['error_msg'] = $msg;
                $this->load->view('modals/master', $data);
            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function pay_tvsubscr_dstv($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/pay_tvsubscr_dstv';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'DSTV Subscription';

            $data['title'] = "MobiCore - Client Pay DSTV";

            $this->form_validation->set_rules('dstv_acc', 'DSTV account', 'trim|required|xss_clean|required');
            $this->form_validation->set_rules('bouquet_choice', 'Bouquet Choice', 'trim|required|xss_clean', 'required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean|required');
            $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|xss_clean|required');

            if ($this->form_validation->run() === FALSE) {
                $this->pay_tvsubscr_menu();
            } else {
                $smartcardNo = $this->input->post('dstv_acc');
                $bouquet = $this->input->post('bouquet_choice');
                $amount = $this->input->post('amount');
                $phonenumber = $this->input->post('phone');

                if (isset($smartcardNo)) {

                    $principal = $_SESSION['Username'];
                    $princ_type = "USER";
                    $action = "verify";

                    //LOAD MERCHANT DETAILS
                    $merchant = $this->merchant_model->getMerchant("USER", "payway");
                    $url = $this->merchant_model->merchantapiurl;
                    $vendor_password = $this->merchant_model->merchantapipassword;
                    $vendor_code = $this->merchant_model->merchantapiusername;


                    if (!isset($url) && !isset($vendor_password) && !isset($vendor_code)) {
                        $msg = '<font color=red>Error message: The system is down please try again later. We apologize for any inconveniences.</font> <br />';
                        $this->pay_tvsubscr_menu($msg);
                    }

                    if ($action == "verify") {
                        $verify_dstv = $this->vas_model->query_tv_customer($smartcardNo, $url, $vendor_code, $vendor_password);
                        $val6 = strstr($verify_dstv, 'Success');
                        if ($val6) {
                            $data['phone_number'] = $phonenumber;
                            $data['amount'] = $amount;
                            $data['dstv_cust_name'] = $_SESSION['dstv_cust_name'];
                            $_SESSION['amount'] = $amount;
                            $_SESSION['bouquet'] = $bouquet;
                            $_SESSION['smartcard_no'] = $smartcardNo;
                            $_SESSION['recno'] = $phonenumber;
                            //$data['content'] = 'client_modals/client_tvsubscr_dstv_fin';
                            $this->login_model->getAccountStatus();
                            //$this->load->view('modals/master', $data);

                        } else {
                            $msg = '<font color=red>Error message: ' . $verify_dstv . ' Please contact the administrator.</font> <br />';
                            $this->pay_tvsubscr_menu($msg);
                        }


                    }

                }

                $data['content'] = 'client_modals/client_tvsubscr_dstv_fin';
                $data['error_msg'] = $msg;
                $this->load->view('modals/master', $data);
            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }


    public function pay_tvsubscr_confirm()
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/pay_tvsubscr';
            $data['again'] = 'index.php/client/pay_tvsubscr';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'DSTV Subscription';

            $data['title'] = "MobiCore - Client Pay DSTV";

            $smartCardNo = $_SESSION['smartcard_no'];
            $amount = $_SESSION['amount'];
            $recnumber = $_SESSION['recno'];

            $principal = $_SESSION['Username'];
            $princ_type = "USER";
            // Replaced by Abdu
            // $transType = 55;
            $transType = 87;

            $paywayNumber = "payway";
            $action = "pay";
            $princtype = "USER";
            $description = "DSTV/GOTV PAYMENT FOR " . $_SESSION['smartcard_no'];
            //LOAD MERCHANT DETAILS
            $merchant = $this->merchant_model->getMerchant("USER", "payway");
            $url = $this->merchant_model->merchantapiurl;
            $vendor_password = $this->merchant_model->merchantapipassword;
            $vendor_code = $this->merchant_model->merchantapiusername;
            $bouquet = $_SESSION['bouquet'];

            if (!isset($url) && !isset($vendor_password) && !isset($vendor_code)) {
                $msg = '<font color=red>Error message 1: The system is down please try again later. We apologize for any inconveniences.</font> <br />';
                $this->pay_tvsubscr_menu($msg);
            }


            if ($action == "pay") {
                $buy_dstv = $this->agent_model->tansaction_process($paywayNumber, $princtype, $amount, $transType, $description);

                if (!isset($buy_dstv->return->status)) {
                    $msg = '<font color=red>Error message 2: The system is down please try again later. We apologize for any inconveniences.</font> <br />';
                    $this->pay_tvsubscr_menu($msg);
                }

                if (isset($buy_dstv->return->status) && $buy_dstv->return->status == 'PROCESSED') {
                    $val7 = strstr($this->vas_model->query_tv_customer($_SESSION['smartcard_no'], $url, $vendor_code, $vendor_password), 'Success');
                    if ($val7) {

                        if ($_SESSION['tv'] == 'DSTV') {
                            $call_dstv = $this->vas_model->loadDstv($amount, $smartCardNo, $recnumber, $buy_dstv->return->transfer->id, $bouquet);
                        } else if ($_SESSION['tv'] == 'GOtv') {
                            $call_gotv = $this->vas_model->loadGotv($amount, $smartCardNo, $recnumber, $buy_dstv->return->transfer->id, $bouquet);
                        }
                        $val8 = "";
                        $val9 = "";

                        if (isset($call_dstv)) {
                            $val8 = strstr($call_dstv, 'Success');
                            $val9 = strstr($call_dstv, 'Accepted');
                        } else if (isset($call_gotv)) {
                            $val8 = strstr($call_gotv, 'Success');
                            $val9 = strstr($call_gotv, 'Accepted');
                        }
                        if ($val8 || $val9) {
                            if (isset($call_dstv)) {
                                $_SESSION['resp'] = $call_dstv;
                            } else if (isset($call_gotv)) {
                                $_SESSION['resp'] = $call_gotv;
                            }

                            $data['content'] = 'client_modals/client_tvsubscr_suc';
                            $this->login_model->getAccountStatus();
                            $this->load->view('modals/master', $data);
                        } else {
                            if (isset($call_dstv)) {
                                $cb = $this->client_model->chargeBack($buy_dstv->return->transfer->id);
                                $msg = '<font color=red>Error message 3: ' . $call_dstv . ' Please contact the administrator.</font> <br />';
                                $this->pay_tvsubscr_menu($msg);
                                return;
                            } else if (isset($call_gotv)) {
                                $cb = $this->client_model->chargeBack($buy_dstv->return->transfer->id);
                                $msg = '<font color=red>Error message 4: ' . $call_gotv . ' Please contact the administrator.</font> <br />';
                                $this->pay_tvsubscr_menu($msg);
                                return;
                            }

                        }

                    } else {
                        $cb = $this->client_model->chargeBack($buy_dstv->return->transfer->id);
                        $msg = '<font color=red>Customer not found</font> <br />';
                        $this->pay_tvsubscr_menu($msg);
                    }
                } else if (isset($buy_dstv->return->status) && $buy_dstv->return->status != "PROCESSED") {
                    $msg = '<font color=red>Error message 5: ' . $buy_dstv->return->status . ' Please contact the administrator.</font> <br />';
                    $this->pay_tvsubscr_menu($msg);

                } else if (isset($buy_dstv->faultstring)) {
                    $msg = '<font color=red>Error message 6: ' . $buy_dstv->faultstring . ' Please contact the administrator.</font> <br />';
                    $this->pay_tvsubscr_menu($msg);
                }
            }

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function pay_tvsubscr($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/pay_tvsubscr';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'DSTV Subscription';

            $data['title'] = "MobiCore - Client Pay DSTV";
            $data['content'] = 'client_modals/client_tvsubscr_st';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function pay_tvsubscr_process($msg = NULL)
    {

        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/pay_tvsubscr';
            //$data['again'] = 'index.php/client/buyumeme';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'DSTV Subscription';

            $data['title'] = "MobiCore - Client Pay DSTV";
            //$data['content'] = 'client_modals/client_buyumeme_fin';
            $this->form_validation->set_rules('smartcard_no', 'Smart Card Number', 'trim|required|xss_clean', 'required');
            $this->form_validation->set_rules('receiver_number', 'Receiver Phone Number', 'trim|required|xss_clean|alpha_numeric', 'required');
            if ($this->form_validation->run() === FALSE) {
                $this->buyumeme();
            } else {
                $smartcardNo = $this->input->post('smartcard_no');
                $recnumber = $this->input->post('receiver_number');
                $_SESSION['rec_number'] = $recnumber;
                $_SESSION['smartcard_no'] = $smartcardNo;
                //$agt_pin = $this->input->post('agent_pin');
                //Start the process of sending the info to the DSTV
                if (isset($smartcardNo)) {

                    $principal = $_SESSION['Username'];
                    $princ_type = "USER";
                    //$transType = 55;
                    $paywayNumber = "payway";
                    $action = "verify";
                    $princtype = "USER";
                    $description = "DSTV VAERIFICATION FOR " . $smartcardNo;
                    //LOAD MERCHANT DETAILS
                    $merchant = $this->merchant_model->getMerchant("USER", "payway");
                    $url = $this->merchant_model->merchantapiurl;
                    $vendor_password = $this->merchant_model->merchantapipassword;
                    $vendor_code = $this->merchant_model->merchantapiusername;

                    if (!isset($url) && !isset($vendor_password) && !isset($vendor_code)) {
                        $msg = '<font color=red>Error message: The system is down please try again later. We apologize for any inconveniences.</font> <br />';
                        $this->pay_tvsubscr($msg);
                    }

                    if ($action == "verify") {
                        $verify_dstv = $this->vas_model->query_tv_customer($smartcardNo);
                        $val6 = strstr($verify_dstv, 'Success');
                        if ($val6) {
                            $data['phone_number'] = $principal;
                            $data['content'] = 'client_modals/client_tvsubscr_conf';
                            $this->login_model->getAccountStatus();
                            $this->load->view('modals/master', $data);

                        } else {
                            $msg = '<font color=red>Error message: ' . $verify_dstv . ' Please contact the administrator.</font> <br />';
                            $this->pay_tvsubscr($msg);
                        }


                    }

                }

            }

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function pay_tvsubscr_confirm_old()
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/pay_tvsubscr';
            $data['again'] = 'index.php/client/pay_tvsubscr';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'DSTV Subscription';

            $data['title'] = "MobiCore - Client Pay DSTV";

            $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean|alpha_numeric', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->buyumeme_st();
            } else {
                $amount = $this->input->post('amount');

                if (isset($amount)) {
                    //get a formatted number
                    $principal = $_SESSION['Username'];
                    $princ_type = "USER";
                    $transType = 55;

                    $paywayNumber = "payway";
                    $action = "pay";
                    $princtype = "USER";
                    $description = "DSTV PAYMENT FOR " . $_SESSION['smartcard_no'];
                    //LOAD MERCHANT DETAILS
                    $merchant = $this->merchant_model->getMerchant("USER", "payway");
                    $url = $this->merchant_model->merchantapiurl;
                    $vendor_password = $this->merchant_model->merchantapipassword;
                    $vendor_code = $this->merchant_model->merchantapiusername;

                    if (!isset($url) && !isset($vendor_password) && !isset($vendor_code)) {
                        $msg = '<font color=red>Error message: The system is down please try again later. We apologize for any inconveniences.</font> <br />';
                        $this->pay_tvsubscr($msg);
                    }


                    if ($action == "pay") {
                        $buy_dstv = $this->agent_model->tansaction_process($paywayNumber, $princtype, $amount, $transType, $description);

                        if (!isset($buy_dstv->return->status)) {
                            $msg = '<font color=red>Error message: The system is down please try again later. We apologize for any inconveniences.</font> <br />';
                            $this->pay_tvsubscr($msg);
                        }

                        if (isset($buy_dstv->return->status) && $buy_dstv->return->status == 'PROCESSED') {
                            $val7 = strstr($this->vas_model->query_tv_customer($_SESSION['smartcard_no']), 'Success');
                            if ($val7) {
                                $call_dstv = $this->vas_model->loadDstv($amount, $_SESSION['smartcard_no'], $_SESSION['rec_number'], $buy_dstv->return->transfer->id);
                                $val8 = strstr($call_dstv, 'Success');
                                $val9 = strstr($call_dstv, 'Accepted');
                                if ($val8 || $val9) {
                                    $_SESSION['resp'] = $call_dstv;
                                    $data['content'] = 'client_modals/client_tvsubscr_suc';
                                    $this->login_model->getAccountStatus();
                                    $this->load->view('modals/master', $data);
                                } else {
                                    $cb = $this->client_model->chargeBack($buy_dstv->return->transfer->id);
                                    $msg = '<font color=red>Error message: ' . $call_dstv . ' Please contact the administrator.</font> <br />';
                                    $this->pay_tvsubscr($msg);
                                }

                            } else {
                                $cb = $this->client_model->chargeBack($buy_dstv->return->transfer->id);
                                $msg = '<font color=red>Customer not found</font> <br />';
                                $this->pay_tvsubscr($msg);
                            }
                        } else {
                            $msg = '<font color=red>Error message: ' . $buy_dstv->return->status . ' Please contact the administrator.</font> <br />';
                            $this->pay_tvsubscr($msg);

                        }
                    }

                }

            }

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function mylife($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/mylife';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Subscribe to Mylife';
            $data['again'] = 'index.php/client/mylife';

            $data['title'] = "MobiCore - Client Subscribe To Mylife";
            $data['content'] = 'client_modals/client_mylife';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function mylife_process()
    {

        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/mylife';
            $data['again'] = 'index.php/client/mylife';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Mylife Subscription';

            $data['title'] = "MobiCore - Client Mylife Subscription";

            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('kinname', 'Kin Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('kinmobile', 'Kin Mobile', 'trim|required|xss_clean|alpha_numeric');
            $this->form_validation->set_rules('insplan', 'Ins Plan', 'trim|required|xss_clean');
            $this->form_validation->set_rules('datepicker', 'Date of birth', 'required');
            $this->form_validation->set_rules('interval', 'Interval', 'trim|required|xss_clean|alpha_numeric');

            $mylifeMerchant = 'mylife';

            if ($this->form_validation->run() === FALSE) {
                $this->mylife();
            } else {
                $email = $this->input->post('email');
                $kinname = $this->input->post('kinname');
                $kinmobile = $this->input->post('kinmobile');
                $insPlan = $this->input->post('insplan');
                $dob = $this->input->post('datepicker');
                $interval = $this->input->post('interval');

                if ($interval == "monthly") {
                    switch ($insPlan) {
                        case 0:
                            $amount = 2500;
                            $descInsPlan = "PA 1,333,333";
                            break;
                        case 1:
                            $amount = 6250;
                            $descInsPlan = "PA 3,333,333";
                            break;
                        case 2:
                            $amount = 12500;
                            $descInsPlan = "PA 6,666,666";
                            break;
                        default:
                            throw new Exception("KO Invalid insurance plan selected");
                            break;
                    }
                } elseif ($interval == "bi-annual") {

                    switch ($insPlan) {
                        case 0:
                            $amount = 14900;
                            $descInsPlan = "PA 1,333,333";
                            break;
                        case 1:
                            $amount = 37200;
                            $descInsPlan = "PA 3,333,333";
                            break;
                        case 2:
                            $amount = 74300;
                            $descInsPlan = "PA 6,666,666";
                            break;
                        default:
                            throw new Exception("KO Invalid insurance plan selected");
                            break;
                    }
                } elseif ($interval == "annual") {
                    switch ($insPlan) {
                        case 0:
                            $amount = 29800;
                            $descInsPlan = "PA 1,333,333";
                            break;
                        case 1:
                            $amount = 74300;
                            $descInsPlan = "PA 3,333,333";
                            break;
                        case 2:
                            $amount = 148600;
                            $descInsPlan = "PA 6,666,666";
                            break;
                        default:
                            throw new Exception("KO Invalid insurance plan selected");
                            break;
                    }
                }

                $princtype = "USER";
                $principal = $_SESSION['Username'];
                $transType = 53;
                $description = "mylife subscription FOR $principal";

                $clientId = $this->client_model->getMember($princtype, $principal);


                //$data = $mylife_request->doPayment();
                //do the normal web do payment here
                $subscribe_mylife = $this->agent_model->tansaction_process($mylifeMerchant, $princtype, $amount, $transType, $description);
                if (!isset($subscribe_mylife->faultstring)) {
                    if ($subscribe_mylife->return->status == "PROCESSED" && isset($subscribe_mylife->return->status)) {
                        $output = $this->vas_model->liberty_enroll($amount, $insPlan, $descInsPlan, $interval, $kinname, $kinmobile, $dob, $clientId, $amount);
                        if ($output == "FAILED") {
                            $cb = $this->client_model->chargeBack($subscribe_mylife->return->transfer->id);
                            $result = $clientId->return->name;
                            $msg = '<font color=red>Error message: ' . $output . " " . $result . ' You already have a mylife account.</font> <br />';
                            $this->mylife($msg);


                        } else if ($output == "SUCCESS") {
                            //process agent and broker percentage
                            $result = "Mylife enrolment was successful.";
                            $_SESSION['result'] = $result;
                            $data['content'] = 'client_modals/client_mylife_suc';
                            $this->login_model->getAccountStatus();
                            $this->load->view('modals/master', $data);
                        }

                    } else {
                        $msg = '<font color=red>Error message: ' . $subscribe_mylife->return->status . ' Please contact the Administrator.</font> <br />';
                        $this->mylife($msg);
                    }
                } else {
                    $msg = '<font color=red>Error message: FAILED ' . $subscribe_mylife->faultstring . ' Please contact the Administrator.</font> <br />';
                    $this->mylife($msg);
                }

            }
        } else {
            redirect(base_url() . 'index.php/login');
        }


    }


    public function transfer_to_bank($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/transfer_to_bank';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Transfer To Bank';
            $data['again'] = 'index.php/client/transfer_to_bank';

            $data['title'] = "MobiCore - Client Transfer To Bank";
            $data['content'] = 'client_modals/client_sendtobank';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }


    public function transfer_to_bank_process($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/transfer_to_bank';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Transfer To Bank';
            $data['again'] = 'index.php/client/transfer_to_bank';
            $data['title'] = "MobiCore - Client Transfer To Bank";
            $data['error_msg'] = $msg;

            $this->form_validation->set_rules('transfer_type', 'Transfer Type', 'trim|required|alpha_numeric|xss_clean');
            $this->form_validation->set_rules('bank', 'Bank', 'trim|required|xss_clean');
            $this->form_validation->set_rules('branch', 'Branch', 'trim|required|xss_clean');
            $this->form_validation->set_rules('beneficiary_name', 'Beneficiary Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('account_number', 'Account Number', 'trim|required|alpha_numeric|xss_clean');
            $this->form_validation->set_rules('conf_account_number', 'Confirm Account Number', 'matches[account_number]|trim|required|xss_clean|alpha_numeric');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean|alpha_numeric');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');

            $bankTransferAgent = 'banktransfer';
            $transType = 33;
            $princtype = "USER";
            $principal = $_SESSION['Username'];
            $description = "Request for bank transfer for $principal";

            if ($this->form_validation->run() === FALSE) {
                $this->transfer_to_bank();
            } else {
                $transferTypeBank = $this->input->post('transfer_type');
                $bank = $this->input->post('bank');
                $branch = $this->input->post('branch');
                $beneficiary_name = $this->input->post('beneficiary_name');
                $account_number = $this->input->post('account_number');
                $amount = $this->input->post('amount');
                $clientEmail = $this->input->post('email');

                if (isset($bank) && isset($amount) && isset($account_number)) {

                    if ($transferTypeBank == "RTGS") {

                        $amount = $amount + 20000;

                    } else if ($transferTypeBank == "EFT") {

                        $amount = $amount + 5000;

                    }

                    $bankTransfer = $this->agent_model->tansaction_process($bankTransferAgent, $princtype, $amount, $transType, $description);

                }

                if (isset($bankTransfer->return->status) && $bankTransfer->return->status == "PROCESSED") {


                    $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://smtp.googlemail.com',
                        'smtp_port' => 465,
                        'smtp_user' => 'banktransfer@mcash.ug',
                        'smtp_pass' => 'Bank2015',
                        'mailtype' => 'html',
                        'charset' => 'iso-8859-1',
                        'wordwrap' => TRUE
                    );

                    $message = "Please transfer funds of amount: UGX$amount, from mcash account: $principal to bank account: $account_number held by: $beneficiary_name with email: $clientEmail attached to bank: $bank and branch: $branch by method: $transferTypeBank.";
                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");
                    $this->email->from('cuthbert@mcash.ug');
                    $this->email->to('martin@mcash.ug');
                    $this->email->cc('ivan@mcash.ug, pmugabi@mcash.ug, annet@mcash.ug, edgar@mcash.ug, iamcutho@gmail.com');
                    $this->email->subject('Request for transfer for funds from mcash account to bank account on mobicore.mcash.ug.');
                    $this->email->message($message);
                    if ($this->email->send()) {
                        $data['success'] = "Your request was successfully recieved, please wait for confirmation.";
                        $data['content'] = "client_modals/client_sendtobank_suc";
                        $this->login_model->getAccountStatus();
                        $this->load->view('modals/master', $data);
                        //echo 'Email sent.';
                    } else {
                        $cb = $this->client_model->chargeBack($bankTransfer->return->transfer->id);
                        $msg = '<font color=red>Error message: ' . show_error($this->email->print_debugger()) . ' Please contact the Administrator.</font> <br />';
                        $this->transfer_to_bank($msg);

                    }


                } else if ($bankTransfer->return->status != "PROCESSED") {
                    $msg = '<font color=red>Error message: ' . $bankTransfer->return->status . ' Please contact the Administrator.</font> <br />';
                    $this->transfer_to_bank($msg);
                } else if (isset($bankTransfer->fautlstring)) {
                    $msg = '<font color=red>Error message: ' . $bankTransfer->faultstring . ' Please contact the Administrator.</font> <br />';
                    $this->transfer_to_bank($msg);
                }
            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }


    public function pay_etax($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/pay_etax';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Pay Tax';
            $data['again'] = 'index.php/client/pay_etax';

            $data['title'] = "MobiCore - Client Pay Tax";
            $data['content'] = 'client_modals/client_payetax';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function payetax_checkref($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/pay_etax';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Pay Tax';
            $data['again'] = 'index.php/client/pay_etax';

            $data['title'] = "MobiCore - Client Pay Tax";
            $this->form_validation->set_rules('rra_ref_id', 'RRA Reference ID', 'trim|required');

            if ($this->form_validation->run() === FALSE) {
                $this->pay_etax();
            } else {
                $rra_param = $this->input->post('rra_ref_id');

                $response = $this->vas_model->getDec($rra_param);

                $xml_return = (array)simplexml_load_string($response->getDecReturn);

                if (isset($xml_return['DECLARATION'])) {
                    $array_response = array();
                    foreach ($xml_return['DECLARATION'] as $key => $Entry) {
                        $array_response[$key] = $Entry;
                    }

                    $data['bank_name'] = $xml_return['@attributes']['ID'];
                    $data['array_response'] = $array_response;
                    $data['content'] = 'client_modals/client_confetax';
                    $this->load->view('modals/master', $data);
                } else {
                    $msg = '<font color=red>Your reference number is not found.</font> <br />';
                    $this->pay_etax($msg);
                }
            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function confpay_etax($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/pay_etax';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Pay Tax';
            $data['again'] = 'index.php/client/pay_etax';

            $data['title'] = "MobiCore - Client Pay Tax";

            $this->load->library('xmlrpc');

            $ch = curl_init();
            $this->form_validation->set_rules('account', 'Mode of Payment', 'trim|required|xss_clean|callback_check_amount|callback_check_pin');
            $this->form_validation->set_rules('pin', 'Enter Pin', 'trim|required|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $data['content'] = 'client_modals/client_confetax';
                $this->load->view('modals/master', $data);
            } else {

                $rra_ref_id = $this->input->post('rra_ref_id');
                $bank_ref = 10078901;
                $rra_bank_acct_no = $this->input->post('rra_bank_acct_no');
                $dec_id = $this->input->post('dec_id');
                $tin = $this->input->post('tin');
                $tax_payer_name = $this->input->post('tax_payer_name');
                $amount_paid = $this->input->post('amount_to_pay');
                $rra_origin_no = $this->input->post('rra_origin_no');
                $assess_no = $this->input->post('assess_no');
                $dec_date = $this->input->post('dec_date');
                $account = $this->input->post('account');
                $pay_date = date('d/m/Y H:i:s');
                $trans_date = date('d/m/Y H:i:s');

                $pay_doc_no = $this->pay_doc_no();

                $rra_str = '<?xml version="1.0" encoding="UTF-8" standalone="no"?><PAYMENT><RRA_REF>' . $rra_ref_id . '</RRA_REF><TIN>' . $tin . '</TIN><TAX_PAYER_NAME>' . $tax_payer_name . '</TAX_PAYER_NAME><PAY_DATE>' . $pay_date . '</PAY_DATE><AMOUNT_PAID>' . $amount_paid . '</AMOUNT_PAID><BANK_REF>' . $bank_ref . '</BANK_REF><RRA_BANK_ACCT_NO>' . $rra_bank_acct_no . '</RRA_BANK_ACCT_NO><PAYMENT_TYPE_NO>1</PAYMENT_TYPE_NO><PAY_DOC_NO>' . $pay_doc_no . '</PAY_DOC_NO><TRANS_DATE>' . $trans_date . '</TRANS_DATE><ASSESS_NO>' . $assess_no . '</ASSESS_NO><RRA_ORIGIN_NO>' . $rra_origin_no . '</RRA_ORIGIN_NO><DEC_ID>' . $dec_id . '</DEC_ID></PAYMENT>';

                $str_save = "<PAYMENT><RRA_REF>$rra_ref_id</RRA_REF><TIN>$tin</TIN><TAX_PAYER_NAME>$tax_payer_name</TAX_PAYER_NAME><PAY_DATE>$pay_date</PAY_DATE><AMOUNT_PAID>$amount_paid</AMOUNT_PAID><BANK_REF>$bank_ref</BANK_REF><RRA_BANK_ACCT_NO>$rra_bank_acct_no</RRA_BANK_ACCT_NO><PAYMENT_TYPE_NO>1</PAYMENT_TYPE_NO><PAY_DOC_NO>$pay_doc_no</PAY_DOC_NO><TRANS_DATE>$trans_date</TRANS_DATE><ASSESS_NO>$assess_no</ASSESS_NO><RRA_ORIGIN_NO>$rra_origin_no</RRA_ORIGIN_NO><DEC_ID>$dec_id</DEC_ID></PAYMENT>";

                $data_insert['rra_ref_id'] = $rra_ref_id;
                $data_insert['pay_doc_no'] = $pay_doc_no;
                $data_insert['transaction_string'] = $str_save;


                $response = $this->vas_model->setPay($rra_str);

                if ($response->setPayReturn != 0) {
                    //$this->db->insert('itax_data',$data_insert);
                    $data['amount'] = $amount_paid;
                    $data['tax_payer_name'] = $tax_payer_name;
                    $data['rra_ref_id'] = $rra_ref_id;
                    $data['content'] = 'client_modals/client_confetax_success';
                    $this->load->view('modals/master', $data);

                } else {
                    $this->pay_etax('There is a problem from RRA Server function. Please try again');

                }
            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function transfer_to_client($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/transfer_to_client';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Transfer To Client';
            $data['again'] = 'index.php/client/transfer_to_client';

            $data['title'] = "MobiCore - Client To Client";
            $data['content'] = 'client_modals/client_transfer';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function transfer_to_unregistered($msg = NULL)
    {
      if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
          $_SESSION['LAST_ACTIVITY'] = time();
          $data['role'] = $_SESSION['role'];
          $data['username'] = $_SESSION['realname'];
          $data['link'] = 'index.php/client/transfer_to_unregistered';
          $data['home'] = 'index.php/client';
          $data['operation'] = 'Transfer To Unregistered Client';
          $data['again'] = 'index.php/client/transfer_to_unregistered';

          $data['title'] = "MobiCore - Client To Client";
          $data['content'] = 'client_modals/client_transfer_unregistered';

          $data['error_msg'] = $msg;
          $this->load->view('modals/master', $data);
      } else {
          redirect(base_url() . 'index.php/login');
      }
    }


    public function client_transfer_verify($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/transfer_to_client';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Transfer To Client';
            $data['again'] = 'index.php/client/transfer_to_client';

            $data['title'] = "MobiCore - Client To Client";
            $this->form_validation->set_rules('receiver_number', 'Receiver Number', 'trim|required|alpha_numeric');
            $this->form_validation->set_rules('conf_rec', 'Confirm Receiver Number', 'trim|required|xss_clean|alpha_numeric');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean|alpha_numeric');

            if ($this->form_validation->run() === FALSE) {
                $this->transfer_to_client();
            } else {
                $recnumber = $this->input->post('receiver_number');
                $confrec = $this->input->post('conf_rec');
                $amount = $this->input->post('amount');

                $memberDetails = $this->client_model->getMember("USER", $recnumber);
                if (isset($memberDetails->return->name)) {
                    $name = $memberDetails->return->name;
                    $_SESSION['name'] = $name;
                    $data['name'] = $name;
                }
                $_SESSION['recno'] = $recnumber;

                $_SESSION['amount'] = $amount;
                $data['error_msg'] = $msg;
                $data['recno'] = $recnumber;
                $data['amount'] = $amount;
                $data['content'] = 'client_modals/client_transfer_verify';
                $this->load->view('modals/master', $data);
            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function client_transfer_process($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/transfer_to_client';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Transfer To Client';
            $data['again'] = 'index.php/client/transfer_to_client';
            $data['title'] = "MobiCore - Client To Client";

            $recnumber = $_SESSION['recno'];
            $amount = $_SESSION['amount'];

            $transType = NULL;
            $princtype = "USER";
            $principal = $_SESSION['Username'];
            $description = "Transfer of funds from $principal to $recnumber";
            $transType = 34;
            if ($amount < 500) {
                $msg = '<font color=red>Error message: amount cannot be less than 500.</font> <br />';
                $this->transfer_to_client($msg);
                return;
            }
            $client_transfer = $this->agent_model->tansaction_process($recnumber, $princtype, $amount, $transType, $description);

            if (!isset($client_transfer)) {
                $msg = '<font color=red>Error message: The system is down, please try again later.</font> <br />';
                $this->transfer_to_client($msg);
                return;
            }

            if (isset($client_transfer->return->status) && $client_transfer->return->status == 'PROCESSED') {

                $data['content'] = 'client_modals/client_transfer_success';
                $data['amount'] = $amount;
                $data['recnumber'] = $recnumber;
                $this->login_model->getAccountStatus();
                $this->load->view('modals/master', $data);


            } else {
                $msg = '<font color=red>Error message: ' . $client_transfer->return->status . '.</font> <br />';
                $this->transfer_to_client($msg);
                return;
            }
            if (isset($client_transfer->faultstring)) {
                $msg = '<font color=red>Error message: ' . $client_transfer->faultstring . '.</font> <br />';
                $this->transfer_to_client($msg);
                return;
            }

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function client_unregistered_transfer_process($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/transfer_to_unregistered';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Transfer To Unregistered Client';
            $data['again'] = 'index.php/client/transfer_to_unregistered';
            $data['title'] = "MobiCore - Client To Unregistered Client";

            $this->form_validation->set_rules('receiver_number', 'Receiver Number', 'trim|required|alpha_numeric');
            $this->form_validation->set_rules('conf_rec', 'Confirm Receiver Number', 'trim|required|xss_clean|alpha_numeric');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean|alpha_numeric');
            $this->form_validation->set_rules('secret_code', 'Secret Code', 'trim|required|xss_clean|alpha_numeric');

            if ($this->form_validation->run() === FALSE) {
                $this->transfer_to_unregistered();
            } else {
                $recnumber = $this->input->post('receiver_number');
                $confrec = $this->input->post('conf_rec');
                $amount = $this->input->post('amount');
                $secret_code = $this->input->post('secret_code');

                $memberDetails = $this->client_model->getMember("USER", $recnumber);
                if (isset($memberDetails->return->name)) {
                    $name = $memberDetails->return->name;
                    $_SESSION['name'] = $name;
                    $data['name'] = $name;
                }
                $_SESSION['recno'] = $recnumber;

                $_SESSION['amount'] = $amount;
                $_SESSION['secret_code'] = $secret_code;
                $data['error_msg'] = $msg;
                $data['recno'] = $recnumber;
                $data['amount'] = $amount;
            }

            $recnumber = $_SESSION['recno'];
            $amount = $_SESSION['amount'];
            $secret_code = $_SESSION['secret_code'];

            $transType = NULL;
            $princtype = "USER";
            $principal = $_SESSION['Username'];
            $description = "Transfer of funds from $principal to $recnumber";
            $transType = 83;
            if ($amount < 500) {
                $msg = '<font color=red>Error message: amount cannot be less than 500.</font> <br />';
                $this->transfer_to_unregistered($msg);
                return;
            }
            $client_transfer = $this->agent_model->tansaction_process("unregistered", $princtype, $amount, $transType, $description);

            if (!isset($client_transfer)) {
                $msg = '<font color=red>Error message: The system is down, please try again later.</font> <br />';
                $this->transfer_to_client($msg);
                return;
            }

            if (isset($client_transfer->return->status) && $client_transfer->return->status == 'PROCESSED') {

                $data['content'] = 'client_modals/client_transfer_success';
                $data['amount'] = $amount;
                $data['recnumber'] = $recnumber;
                $transid = $client_transfer->return->transfer->id;
                $this->sendsms($recnumber, $transid, $secret_code, $amount);

                $transaction_data = array(
          				'sender' => $_SESSION['Username'],
          				'recipient' => $recnumber,
          				'amount' => $amount,
                  'sender_transaction_id' => $transid,
                  'secret_code' => $secret_code,
                  'status' => 'Pending',
          			);
                $this->unregistered_client_transfers->save_transaction($transaction_data);
                $this->login_model->getAccountStatus();
                $this->load->view('modals/master', $data);


            } else {
                $msg = '<font color=red>Error message: ' . $client_transfer->return->status . '.</font> <br />';
                $this->transfer_to_client($msg);
                return;
            }
            if (isset($client_transfer->faultstring)) {
                $msg = '<font color=red>Error message: ' . $client_transfer->faultstring . '.</font> <br />';
                $this->transfer_to_unregistered($msg);
                return;
            }

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function sendsms($clientno, $transid, $secretcode, $amount) {
      $mobile  = $_SESSION['Username'];
      $message = urlencode("You have recieved UGX ".$amount." from ".$mobile." Transaction ID: " . $transid." and Secret Code is ".$secretcode);

      $this->sms_model->phoneNumber = $clientno;
      $this->sms_model->msg = $message;

      $sms = $this->sms_model->sendMessage();

      return $sms;
    }

    public function check_amount($account)
    {
        if ($account == 1) {
            $username = $this->input->post('mcash_username');
            $amount = $this->input->post('amount_to_pay');
            $princType = "USER";
            $account_type = 9;
            $response = $this->client_model->check_balance($username, $princType, $account_type);
            $isExist = $response->return;
            if (isset($isExist)) {
                $balance = $isExist->accountStatus->balance;
                if (isset($balance) && $balance >= $amount) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('account', 'You have insufficient balance to procced');
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('account', 'Parameter Error with Username.');
                return FALSE;
            }

        } elseif ($account == 2) {
            $username = $_SESSION['Username'];
            $amount = $this->input->post('amount_to_pay');
            $princType = $_SESSION['principalType'];
            $account_type = 8;
            $response = $this->client_model->check_balance($username, $princType, $account_type);
            $isExist = $response->return;
            if (isset($isExist)) {
                $balance = $isExist->accountStatus->balance;
                if (isset($balance) && $balance >= $amount) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('account', 'You have insufficient balance to procced');
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('account', 'Parameter Error with Username.');
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function check_pin($account)
    {
        if ($account == 1) {
            $username = $this->input->post('mcash_username');
            $pin = $this->input->post('pin');
            $princType = "USER";
            $checked = $this->login_model->verify_pin($username, $princType, $pin);
            if ($checked == TRUE) {
                return TRUE;
            } else {
                $this->form_validation->set_message('account', 'Your credentials are wrong. Please try again.');
                return FALSE;
            }

        } elseif ($account == 2) {
            $username = $_SESSION['Username'];
            $princType = $_SESSION['principalType'];
            $pin = $this->input->post('pin');
            $checked = $this->login_model->verify_pin($username, $princType, $pin);
            if ($checked == TRUE) {
                return TRUE;
            } else {
                $this->form_validation->set_message('account', 'Your credentials are wrong. Please try again.');
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function pay_doc_no()
    {
        $con = mysql_connect('localhost', 'root', 'mobicash');
        $rv = mysql_select_db('itax', $con);

        $retrieved = mysql_query("select MAX(pay_doc_no) from itax_doc_no");
        while ($row = mysql_fetch_assoc($retrieved)) {
            $pay_doc_no = $row['MAX(pay_doc_no)'];
        }
        if (isset($pay_doc_no) == null) {
            mysql_query("insert into itax_doc_no (pay_doc_no) VALUES ('10010001')");
            return '10010001';
        } else {
            mysql_query("insert into itax_doc_no (pay_doc_no) VALUES ('" . ($pay_doc_no + 1) . "')");
            return ($pay_doc_no + 1);
        }
    }

    public function client_report()
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/client_report';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Client Report';
            $data['again'] = 'index.php/client/client_report';

            $data['title'] = "MobiCore - Client Report";

            $principal = $_SESSION['Username'];

            $report = $this->agent_model->searchAccountHistory("USER", $principal);

            //$data['report'] = array_reverse($report->return->transfers);
            $data['report'] = $report->return->transfers;

            $data['content'] = 'client_modals/client_report';

            //$data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }

    }

    public function webschoolpay()
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/webschoolpay';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'School Fees Payment';
            $data['again'] = 'index.php/client/webschoolpay';

            $data['title'] = "MobiCore - School Fees Payment";

            $principal = $_SESSION['Username'];

            $data['content'] = 'client_modals/client_webschoolpay';

            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }

    }

    public function smile_menu($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/smile_menu';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Smile Menu';
            $data['again'] = 'index.php/client/smile_menu';

            $data['title'] = "MobiCore - Smile Menu";
            $data['content'] = 'client_modals/smile_menu';

            $data['error_msg'] = $msg;


            $this->load->view('modals/master', $data);
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function smile_buyairtime($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/smile_buyairtime';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Smile Airtime Purchase';
            $data['again'] = 'index.php/client/smile_buyairtime';

            $data['title'] = "MobiCore - Smile Buy Airtime";
            $data['content'] = 'client_modals/smile_buyairtime_st';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function smile_buyairtime_process($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/smile_buyairtime';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Smile Menu';
            $data['again'] = 'index.php/client/smile_buyairtime';

            $data['title'] = "MobiCore - Smile Buy Airtime";

            $this->form_validation->set_rules('account_id', 'Account Id', 'trim|required|alpha_numeric');

            if ($this->form_validation->run() === FALSE) {
                $this->smile_buyairtime();
            } else {
                $accountId = $this->input->post('account_id');

                $_SESSION['account_id'] = $accountId;

                $merchant = $this->merchant_model->getMerchant("USER", "smile");

                $url = $this->merchant_model->merchantapiurl;
                $vendor_password = $this->merchant_model->merchantapipassword;
                $vendor_code = $this->merchant_model->merchantapiusername;

                $sess = $this->vas_model->smile_authenticate($vendor_code, $vendor_password, $url);

                $customerDetails = $this->vas_model->validate_account($sess, $accountId, $vendor_code, $vendor_password, $url);

                if (!is_array($customerDetails)) {
                    $msg = '<font color=red>Error message: An error was encountered, account id invalid.</font> <br />';
                    $this->smile_buyairtime($msg);
                    return;
                }

                $firstName = $customerDetails[0];
                $middleName = $customerDetails[1];
                $lastName = $customerDetails[2];

                if (strlen($middleName) > 1) {

                    $fullName = $firstName . " " . $middleName . " " . $lastName;

                } else {
                    $fullName = $firstName . " " . $lastName;
                }
                $data['accountId'] = $accountId;
                $data['custname'] = $fullName;
                $data['content'] = 'client_modals/smile_buyairtime_proc';
                $data['error_msg'] = $msg;
                $this->load->view('modals/master', $data);
            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function smile_buyairtime_fin($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/smile_buyairtime';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Smile Menu';
            $data['again'] = 'index.php/client/smile_buyairtime';

            $data['title'] = "MobiCore - Smile Buy Airtime";

            $this->form_validation->set_rules('amount', 'Amount', 'trim|required');

            if ($this->form_validation->run() === FALSE) {
                $this->smile_buyairtime();
            } else {
                $accountId = $_SESSION['account_id'];
                $amount = $this->input->post('amount');

                $recnumber = "smile";
                $description = "Smile client airtime purchase for account id " . $accountId;
                $transType = 70;

                $systemBuyAirtime = $this->agent_model->tansaction_process($recnumber, "USER", $amount, $transType, $description);

                $merchant = $this->merchant_model->getMerchant("USER", "smile");

                $url = $this->merchant_model->merchantapiurl;
                $vendor_password = $this->merchant_model->merchantapipassword;
                $vendor_code = $this->merchant_model->merchantapiusername;

                $sess = $this->vas_model->smile_authenticate($vendor_code, $vendor_password, $url);

                if (isset($systemBuyAirtime->return->status) && $systemBuyAirtime->return->status == "PROCESSED") {
                    $amount = $amount * 100;
                    $buySmileAirtime = $this->vas_model->balance_transfer($sess, $amount, $accountId, $systemBuyAirtime->return->transfer->id, $url);

                    $err = strstr($buySmileAirtime, 'rror');
                    $err2 = strstr($buySmileAirtime, 'ailure');
                    $err3 = strstr($buySmileAirtime, 'xception');
                    if ($err || $err2 || $err3) {
                        $chargeBack = $this->client_model->charge_back($systemBuyAirtime->return->transfer->id);
                        $msg = '<font color=red>Error message: An error was encountered, (' . $buySmileAirtime . '), please contact the administrator.</font> <br />';
                        $this->smile_buyairtime($msg);
                        return;
                    } else {
                        $principal = $_SESSION['Username'];
                        $data['error_msg'] = $msg;
                        $amount = $amount * 0.01;
                        $smsresponse = urlencode("To opt out dial *196#. Hello, we have received your payment of UGX $amount and credited your Smile account with airtime. Thank you for choosing Smile");
                        $this->sms_model->msg = $smsresponse;
                        $this->sms_model->phoneNumber = $principal;

                        $sms = $this->sms_model->sendMessage();
                        $data['content'] = 'client_modals/smile_buyairtime_fin';
                        $this->login_model->getAccountStatus();
                        $this->load->view('modals/master', $data);
                    }
                } else if (isset($systemBuyAirtime->return->status) && $systemBuyAirtime->return->status != "PROCESSED") {
                    $msg = '<font color=red>Error message: An error was encountered, (' . $systemBuyAirtime->return->status . '), please contact the administrator.</font> <br />';
                    $this->smile_buyairtime($msg);
                    return;
                } else if (isset($systemBuyAirtime->faultstring)) {
                    $msg = '<font color=red>Error message: An error was encountered, (' . $systemBuyAirtime->faultstring . '), please contact the administrator.</font> <br />';
                    $this->smile_buyairtime($msg);
                    return;
                }

            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function _smile_buyairtime_process($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/smile_buyairtime';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Smile Airtime Purchase';
            $data['again'] = 'index.php/client/smile_buyairtime';

            $data['title'] = "MobiCore - Smile Buy Airtime";

            $this->form_validation->set_rules('account_id', 'Account Id', 'trim|required|alpha_numeric');
            $this->form_validation->set_rules('conf_acc', 'Account Id', 'trim|required|alpha_numeric|matches[account_id]');
            $this->form_validation->set_rules('amount', 'amount', 'trim|required|alpha_numeric');

            if ($this->form_validation->run() === FALSE) {
                $this->smile_buyairtime();
            } else {
                $accountId = $this->input->post('account_id');
                $amount = $this->input->post('amount');

                $recnumber = "smile";
                $description = "Smile client airtime purchase for account id " . $accountId;
                $transType = 70;
                $systemBuyAirtime = $this->agent_model->tansaction_process($recnumber, "USER", $amount, $transType, $description);

                $merchant = $this->merchant_model->getMerchant("USER", "smile");

                $url = $this->merchant_model->merchantapiurl;
                $vendor_password = $this->merchant_model->merchantapipassword;
                $vendor_code = $this->merchant_model->merchantapiusername;

                $sess = $this->vas_model->smile_authenticate($vendor_code, $vendor_password, $url);
                if (isset($systemBuyAirtime->return->status) && $systemBuyAirtime->return->status == "PROCESSED") {
                    $amount = $amount * 100;
                    $buySmileAirtime = $this->vas_model->balance_transfer($sess, $amount, $accountId, $systemBuyAirtime->return->transfer->id, $url);
                }

                if (isset($systemBuyAirtime->return->status) && $systemBuyAirtime->return->status != "PROCESSED") {
                    $msg = '<font color=red>Error message: An error was encountered, (' . $systemBuyAirtime->return->status . '), please contact the administrator.</font> <br />';
                    $this->smile_buyairtime($msg);
                    return;
                }
                if (isset($systemBuyAirtime->faultstring)) {
                    $msg = '<font color=red>Error message: An error was encountered, (' . $systemBuyAirtime->faultstring . '), please contact the administrator.</font> <br />';
                    $this->smile_buyairtime($msg);
                }
                $err = strstr($buySmileAirtime, 'rror');
                $err2 = strstr($buySmileAirtime, 'xception');
                $err3 = strstr($buySmileAirtime, 'ailure');
                if ($err || $err2 || $err3) {
                    $msg = '<font color=red>Error message: An error was encountered, (' . $buySmileAirtime . '), please contact the administrator.</font> <br />';
                    $chargeback = $this->client_model->chargeBack($systemBuyAirtime->return->transfer->id);
                    $this->smile_buyairitme($msg);
                    return;
                }
                $principal = $_SESSION['Username'];
                $amount = $amount * 0.01;
                $smsresponse = urlencode("To opt out dial *196#. Hello, we have received your payment of UGX $amount and credited your Smile account with smile credit. Thank you for choosing Smile");
                $this->sms_model->msg = $smsresponse;
                $this->sms_model->phoneNumber = $principal;

                $sms = $this->sms_model->sendMessage();

                $data['success'] = "successful";
                $data['error_msg'] = $msg;
                $data['content'] = 'client_modals/smile_buyairtime_suc';
                $this->login_model->getAccountStatus();
                $this->load->view('modals/master', $data);
            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function smile_buybundle($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/smile_buybundle';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Smile Menu';
            $data['again'] = 'index.php/client/smile_buybundle';

            $data['title'] = "MobiCore - Smile Buy Bundle";

            $data['error_msg'] = $msg;

            $data['content'] = 'client_modals/smile_buybundle_st';

            $this->load->view('modals/master', $data);

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function smile_buybundle_process($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/smile_buybundle';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Smile Menu';
            $data['again'] = 'index.php/client/smile_buybundle';

            $data['title'] = "MobiCore - Smile Buy Bundle";

            $this->form_validation->set_rules('account_id', 'Account Id', 'trim|required|alpha_numeric');

            if ($this->form_validation->run() === FALSE) {
                $this->smile_buybundle();
            } else {
                $accountId = $this->input->post('account_id');

                $_SESSION['account_id'] = $accountId;

                $merchant = $this->merchant_model->getMerchant("USER", "smile");

                $url = $this->merchant_model->merchantapiurl;
                $vendor_password = $this->merchant_model->merchantapipassword;
                $vendor_code = $this->merchant_model->merchantapiusername;

                $sess = $this->vas_model->smile_authenticate($vendor_code, $vendor_password, $url);

                $customerDetails = $this->vas_model->validate_account($sess, $accountId, $vendor_code, $vendor_password, $url);

                if (!is_array($customerDetails)) {
                    $msg = '<font color=red>Error message: An error was encountered, account id invalid.</font> <br />';
                    $this->smile_buybundle($msg);
                    return;
                }

                $firstName = $customerDetails[0];
                $middleName = $customerDetails[1];
                $lastName = $customerDetails[2];

                if (strlen($middleName) > 1) {

                    $fullName = $firstName . " " . $middleName . " " . $lastName;

                } else {
                    $fullName = $firstName . " " . $lastName;
                }
                $catalogue = array();
                $catalogue = $this->vas_model->fetch_bundle_catalogue($sess, $url);

                if (strstr($catalogue, 'rror')) {
                    $msg = '<font color=red>Error message: An error was encountered, the bundle catalogue could not be loaded. Please contact the administrator.</font> <br />';
                    $this->smile_buybundle($msg);
                    return;
                }

                $options = array(
                    '' => 'Select',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    '6' => '6',
                    '7' => '7',
                    '8' => '8',
                    '9' => '9',
                    '10' => '10'
                );

                $data['catalogue'] = $catalogue;
                $data['options'] = $options;
                $data['accountId'] = $accountId;
                $data['custname'] = $fullName;
                $data['content'] = 'client_modals/smile_buybundle_proc';
                $data['error_msg'] = $msg;
                $this->load->view('modals/master', $data);
            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function smile_buybundle_fin($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/smile_buybundle';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Smile Menu';
            $data['again'] = 'index.php/client/smile_buybundle';

            $data['title'] = "MobiCore - Smile Buy Bundle";

            $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|alpha_numeric');
            $this->form_validation->set_rules('bundle_choice', 'Bundle Choice', 'trim|required|alpha_numeric');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required');

            if ($this->form_validation->run() === FALSE) {
                $this->smile_buybundle();
            } else {
                $accountId = $_SESSION['account_id'];
                $quantity = $this->input->post('quantity');
                $amount = $this->input->post('amount');
                $bundleChoice = $this->input->post('bundle_choice');

                $recnumber = "smile";
                $description = "Smile client bundle purchase for account id " . $accountId;
                $transType = 70;

                $systemBuyBundle = $this->agent_model->tansaction_process($recnumber, "USER", $amount, $transType, $description);

                $merchant = $this->merchant_model->getMerchant("USER", "smile");

                $url = $this->merchant_model->merchantapiurl;
                $vendor_password = $this->merchant_model->merchantapipassword;
                $vendor_code = $this->merchant_model->merchantapiusername;

                $sess = $this->vas_model->smile_authenticate($vendor_code, $vendor_password, $url);

                if (isset($systemBuyBundle->return->status) && $systemBuyBundle->return->status == "PROCESSED") {
                    $amount = $amount * 100;
                    $buyBundle = $this->vas_model->smile_bundle_buy($sess, $amount, $accountId, $systemBuyBundle->return->transfer->id, $bundleChoice, $quantity, $url);

                    $err = strstr($buyBundle, 'rror');
                    $err2 = strstr($buyBundle, 'ailure');
                    $err3 = strstr($buyBundle, 'xception');
                    if ($err || $err2 || $err3) {
                        $chargeBack = $this->client_model->charge_back($systemBuyBundle->return->transfer->id);
                        $msg = '<font color=red>Error message: An error was encountered, (' . $buyBundle . '), please contact the administrator.</font> <br />';
                        $this->smile_buybundle($msg);
                        return;
                    } else {
                        $principal = $_SESSION['Username'];
                        $data['error_msg'] = $msg;
                        $amount = $amount * 0.01;
                        $smsresponse = urlencode("To opt out dial *196#. Hello, we have received your payment of UGX $amount and credited your Smile account with data. Thank you for choosing Smile");
                        $this->sms_model->msg = $smsresponse;
                        $this->sms_model->phoneNumber = $principal;

                        $sms = $this->sms_model->sendMessage();
                        $data['content'] = 'client_modals/smile_buybundle_fin';
                        $this->login_model->getAccountStatus();
                        $this->load->view('modals/master', $data);
                    }
                } else if (isset($systemBuyBundle->return->status) && $systemBuyBundle->return->status != "PROCESSED") {
                    $msg = '<font color=red>Error message: An error was encountered, (' . $systemBuyBundle->return->status . '), please contact the administrator.</font> <br />';
                    $this->smile_buybundle($msg);
                    return;
                } else if (isset($systemBuyBundle->faultstring)) {
                    $msg = '<font color=red>Error message: An error was encountered, (' . $systemBuyBundle->faultstring . '), please contact the administrator.</font> <br />';
                    $this->smile_buybundle($msg);
                    return;
                }

            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function smile_query_balance($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/smile_buybundle';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Smile Menu';
            $data['again'] = 'index.php/client/smile_buybundle';

            $data['title'] = "MobiCore - Smile Query Balance";
            $data['content'] = 'client_modals/smile_querybalance';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function smile_query_balance_suc($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/smile_buybundle';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Smile Menu';
            $data['again'] = 'index.php/client/smile_buybundle';

            $data['title'] = "MobiCore - Smile Query Balance";
            $this->form_validation->set_rules('account_id', 'Account Id', 'trim|required|alpha_numeric');

            if ($this->form_validation->run() === FALSE) {
                $this->smile_buybundle();
            } else {
                $accountId = $this->input->post('account_id');

                $sess = $this->vas_model->smile_authenticate();

                $balance = $this->vas_model->smile_balance_query($sess, $accountId);

                $err = strstr($balance, 'rror');

                if ($err) {
                    $msg = '<font color=red>Error message: An error was encountered, (' . $balance . '), please contact the administrator.</font> <br />';
                    $this->smile_query_balance($msg);
                    return;
                }

                $data['content'] = 'client_modals/smile_querybalance_suc';
                $data['balance'] = $balance;
                $data['error_msg'] = $msg;
                $this->load->view('modals/master', $data);


            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function buybundles($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/buybundles';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Buy Internet Bundles';
            $data['again'] = 'index.php/client/buybundles';

            $data['title'] = "MobiCore - Buy Internet Bundles";
            $data['content'] = 'client_modals/client_bundles';

            $data['error_msg'] = $msg;

            $options = array(
                '' => 'Select',
                'OrangeInet' => 'ORANGE',
                'WaridInet' => 'WARID',
                'SMILECOMS' => 'SMILE',
                'Sanlam' => 'SANLAM',
                'ForisInet' => 'IN',
                'AIRTEL-INET' => 'AIRTEL'
            );

            $data['options'] = $options;

            $this->load->view('modals/master', $data);

            //$priceList = $this->vas_model->getTvPriceList($provider);

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function buybundles_pricelist($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/buybundles';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Buy Internet Bundles';
            $data['again'] = 'index.php/client/buybundles';
            $data['title'] = "MobiCore - Buy Internet Bundles";

            $this->form_validation->set_rules('provider', 'Provider', 'trim|required|xss_clean');

            if ($this->form_validation->run() === FALSE) {
                $this->buybundles();
            } else {

                $provider = $this->input->post('provider');

                if ($provider == 'SMILECOMS') {
                    redirect('client/smile_menu');
                } else if ($provider == 'WaridInet') {
                    $network = 'WARID-INET';
                    $_SESSION['network'] = $network;
                } else if ($provider == 'OrangeInet') {
                    $network = 'Orange Internet';
                    $_SESSION['network'] = $network;
                } else if ($provider == 'ForisInet') {
                    $network = 'FORIS';
                    $_SESSION['network'] = $network;
                } else if ($provider == 'Sanlam') {
                    $network = 'SANLAM';
                    $_SESSION['network'] = $network;
                } else if ($provider == 'AIRTEL-INET') {
                    $_SESSION['network'] = 'AIRTEL';
                }

                $merchant = $this->merchant_model->getMerchant("USER", "payway");
                $url = $this->merchant_model->merchantapiurl;
                $vendor_password = $this->merchant_model->merchantapipassword;
                $vendor_code = $this->merchant_model->merchantapiusername;

                $priceList = array();
                $priceList = $this->vas_model->getPriceList($provider);

                $data['error_msg'] = $msg;
                $data['priceList'] = $priceList;
                $data['content'] = 'client_modals/client_bundles_proc';
                $this->load->view('modals/master', $data);
            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function buybundles_fin($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/buybundles';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Buy Internet Bundles';
            $data['again'] = 'index.php/client/buybundles';
            $data['title'] = "MobiCore - Buy Internet Bundles";

            $this->form_validation->set_rules('acc_no', 'Bundle Choice', 'trim|required|xss_clean');
            $this->form_validation->set_rules('bundle_choice', 'Bundle Choice', 'trim|required|xss_clean');
            $this->form_validation->set_rules('amount', 'Bundle Choice', 'trim|required|xss_clean');
            $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|xss_clean');

            if ($this->form_validation->run() === FALSE) {
                $this->buybundles();
            } else {
                $principal = $_SESSION['Username'];
                $phonenumber = $this->input->post('phone');
                $account = $this->input->post('acc_no');
                $bundleChoice = $this->input->post('bundle_choice');
                $amount = $this->input->post('amount');
                $network = $_SESSION['network'];

                $transType = 55;
                $description = "Payway bundle load for: " . $principal;

                $merchant = $this->merchant_model->getMerchant("USER", "payway");
                $url = $this->merchant_model->merchantapiurl;
                $paywayNumber = "payway";
                $vendor_password = $this->merchant_model->merchantapipassword;
                $vendor_code = $this->merchant_model->merchantapiusername;
                $loadBundle = array();

                $systemBundleBuy = $this->agent_model->tansaction_process($paywayNumber, "USER", $amount, $transType, $description);

                if (isset($systemBundleBuy->return->status) && $systemBundleBuy->return->status == "PROCESSED") {
                    $loadBundle = $this->vas_model->load_bundles($account, $network, $bundleChoice, $url, $vendor_code, $vendor_password, $amount, $phonenumber, $systemBundleBuy->return->transfer->id);
                    $val = strstr($loadBundle, 'Success');
                    $val2 = strstr($loadBundle, 'Accepted');
                    if ($val || $val2) {
                        $data['error_msg'] = $msg;
                        $data['content'] = 'client_modals/client_bundles_fin';
                        $this->login_model->getAccountStatus();
                        $this->load->view('modals/master', $data);
                    } else {
                        $chargeBack = $this->client_model->chargeBack($systemBundleBuy->return->transfer->id);
                        $msg = '<font color=red>Error message: ' . $loadBundle . ' Please contact the administrator.</font> <br />';
                        $this->buyairtime($msg);
                        return;
                    }
                } else if (isset($systemBundleBuy->return->status) && $systemBundleBuy->return->status != "PROCESSED") {

                    $msg = '<font color=red>Error message: ' . $systemBundleBuy->return->status . ', we apologize for any inconveniences.</font> <br />';
                    $this->buybundles($msg);
                    return;
                } else if (isset($systemBundleBuy->faultstring)) {
                    $msg = '<font color=red>Error message: ' . $systemBundleBuy->faultstring . ', we apologize for any inconveniences.</font> <br />';
                    $this->buybundles($msg);
                    return;
                }

            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function update_email($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/update_email';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Smile Menu';
            $data['again'] = 'index.php/client/update_email';

            $data['title'] = "MobiCore - Smile Update Email";
            $data['content'] = 'client_modals/update_email';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function update_email_process($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/update_email';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Smile Menu';
            $data['again'] = 'index.php/client/update_email';

            $data['title'] = "MobiCore - Smile Update Email";

            $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
            $this->form_validation->set_rules('conf_email', 'Confirm Email', 'trim|required|xss_clean|matches[email]');


            if ($this->form_validation->run() === FALSE) {
                $this->buybundles();
            } else {
                $principal = $_SESSION['Username'];
                $email = $this->input->post('email');

                $update = $this->client_model->updateMember("USER", $principal, $email);
                $data['content'] = 'client_modals/update_email_suc';
                $data['error_msg'] = $msg;
                $this->load->view('modals/master', $data);

            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function client_change_pin($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/client_change_pin';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Change Pin';
            $data['again'] = 'index.php/client/client_change_pin';

            $data['title'] = "MobiCore - Client Change Pin";
            $data['content'] = 'client_modals/client_changepin';

            $data['error_msg'] = $msg;
            $this->load->view('modals/master', $data);

        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    public function client_change_pin_suc($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/client_change_pin';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Change Pin';
            $data['again'] = 'index.php/client/client_change_pin';

            $data['title'] = "MobiCore - Client Change Pin";

            $this->load->helper('security');
            $this->form_validation->set_rules('pin', 'Pin', 'trim|required');
            $this->form_validation->set_rules('conf_pin', 'Confirm Pin', 'trim|required|matches[pin]');

            if ($this->form_validation->run() === FALSE) {
                $this->client_change_pin();
            } else {
                $principal = $_SESSION['Username'];
                $old_pin = $this->input->post('old_pin');
                $pin = $this->input->post('pin');

                $update = $this->client_model->updatePin("USER", $principal, $pin);
                $data['content'] = 'client_modals/client_changepin_suc';
                $data['error_msg'] = $msg;
                $this->load->view('modals/master', $data);

            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }

    //azam menu=============================================================================================
  	public function pay_azam($msg = NULL)
      {
          if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
              $_SESSION['LAST_ACTIVITY'] = time();
              $data['role'] = $_SESSION['role'];
              $data['username'] = $_SESSION['realname'];
              $data['link'] = 'index.php/client/pay_azam';
              $data['home'] = 'index.php/client';
              $data['operation'] = 'Buy Azam TV';

			  
			  //fetch bouquet list
              $bouquet_data = file_get_contents("http://mobicore.mcash.ug/bio-api/listazam.php");
			  $xml = new SimpleXMLElement($bouquet_data);
			  foreach($xml->azam->bouquet as $bouquet){
                
				/* echo $bouquet->name - $bouquet->code - $bouquet->cost . "<br>";
				    
				  
				  */
                  }

              $data['title'] = "MobiCore - Client Home";
              $data['content'] = 'client_modals/client_buyazam';

              $data['error_msg'] = $msg;
              $this->load->view('modals/master', $data);
          } else {
              redirect(base_url() . 'index.php/login');
          }

      }

      public function pay_azam_process($msg = NULL)
      {
           if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
              $_SESSION['LAST_ACTIVITY'] = time();
              $data['role'] = $_SESSION['role'];
              $data['username'] = $_SESSION['realname'];
              $data['link'] = 'index.php/client';
              $data['again'] = 'index.php/client/pay_azam';
              $data['home'] = 'index.php/client';
              $data['operation'] = 'Buy Azam';

              $data['title'] = "MobiCore - Client Azam";

              $this->form_validation->set_rules('bouquet', 'Bouquet', 'trim|required|xss_clean');
              $this->form_validation->set_rules('account_number', 'Account Number', 'trim|required|xss_clean|alpha_numeric');
              $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean|alpha_numeric');
              //$this->form_validation->set_rules('agent_pin','Agent Pin','trim|required|xss_clean|callback_check_agentpin');
              if ($this->form_validation->run() === FALSE) {
                  $this->buy_azam();
              } else {
                  $account = $this->input->post('account_number');
                  $amount = $this->input->post('amount');
                  $bouquet = $this->input->post('bouquet');

  				        //check for account name
              }
          } else {
              redirect(base_url() . 'index.php/login');
          }
    }

    public function print_receipt($msg = NULL)
    {
        if (isset($_SESSION['principalType']) && (time() - $_SESSION['LAST_ACTIVITY'] < 900)) {
            $_SESSION['LAST_ACTIVITY'] = time();
            $data['role'] = $_SESSION['role'];
            $data['username'] = $_SESSION['realname'];
            $data['link'] = 'index.php/client/print_receipt';
            $data['home'] = 'index.php/client';
            $data['operation'] = 'Print Transaction Receipt';
            $data['again'] = 'index.php/client/print_receipt';

            $data['title'] = "MobiCore - Print Receipt";
            $data['content'] = 'client_modals/print_receipt';

            //$data['error_msg'] = $msg;
            //$this->load->view('modals/master', $data);

            $this->form_validation->set_rules('key', 'Key', 'trim|required|xss_clean');

            if ($this->form_validation->run() === FALSE) {
                $this->client_report();
            } else {
                $principal = $_SESSION['Username'];
                $key = $this->input->post('key');

                $values = $_SESSION['key' . $key];
//            echo "<pre>";
//            print_r($values);
//            echo "<pre>";

                $date = $values->formattedProcessDate;
                $clientId = $_SESSION['Username'];

                $client_details = $this->client_model->getMember("USER", $clientId);

                $client_name = urlencode($client_details->return->name);
                $client_email = urlencode($client_details->return->email);

                $details = "Uganda";
                $paymentDetails = urlencode($values->description);
                $transactionid = $values->id;
                $productName = urlencode($values->transferType->name);
                $quantity = 1;
                $amount = str_replace("-", "", $values->formattedAmount);


                $ch = curl_init();
                $source = "http://mobicore.mcash.ug/bio-api/receipting/invoice/ex.php?date=$date&emailaddress=$client_email&clientid=$clientId&clientname=$client_name&clientdetails=$details&paymentname=$paymentDetails&transactionid=$transactionid&product=$productName&quantity=$quantity&amount=$amount";
                curl_setopt($ch, CURLOPT_URL, $source);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $data = curl_exec($ch);
                curl_close($ch);

                $destination = dirname(__FILE__) . '/file.pdf';
                $file = fopen($destination, "w+");
                fputs($file, $data);
                fclose($file);
                $filename = 'receipt.pdf';

                header("Cache-Control: public");
                header("Content-Description: File Transfer");
//header("Content-Disposition: attachment; filename=$filename");
                header("Content-Type: application/pdf");
                header("Content-Transfer-Encoding: binary");
                @readfile($destination);
            }
        } else {
            redirect(base_url() . 'index.php/login');
        }
    }


}

?>
