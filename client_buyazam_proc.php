<section class="section-single">
    	<div class="container">
            <div class="single-post-wrapper col-lg-9 col-md-9 col-sm-12 clearfix">
            
                <div class="tab-services clearfix">
                    <ul id="servicetab" class="nav nav-tabs">
                        <li class="active"><a href="#service1" data-toggle="tab"><i class="glyphicon glyphicon-user"></i><?php echo $username; ?></a></li>
                        <li><a href="<?php echo base_url().'index.php/login/logout' ?>"><i class="glyphicon glyphicon-log-out"></i>Logout</a></li>
                    </ul><!-- servicetab -->
                    
                    <div id="servicetabcontent" class="tab-content">
                        <div class="tab-pane fade in active clearfix" id="service1">
                           <div class="bs-example">                              
                                <div class="form-group">
                                  <label for="status" class="col-lg-2 control-label">Customer Name</label>
                                  <div class="col-lg-10">
                                    <label for="status" class="col-lg-2 control-label"> <?php echo $customer_name; ?>  </label>
                                  </div>
                                </div>
                          </div><!-- /example -->
                                <div class="form-group">
                                <label class="btn btn-primary" ><a href="<?php echo base_url() . $home; ?>" ><u><i class="glyphicon glyphicon-arrow-left"></i> Pay Azam</u></a></label>
                            </div>
			    <!-- end col-lg-8 -->
                        </div>
                       
                    </div><!-- end servicetabcontent -->
                </div><!-- /tab services -->
                 
            </div><!-- single-post-wrapper -->

        </div><!-- end container -->
</section>